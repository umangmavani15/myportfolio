---
title: "About" # in any language you want
layout: "about" # is necessary
language: en
url: "/about"
summary: About

comments: false
---

Hi folks! I am Umang Bharat Mavani, a Computer science undergrad from St. Francis Institute Of Technology, University Of Mumbai in 2019. I am a Software Developement Enginneer by profession and have relavant experience in JAVA, Python, AWS, DevOps, Kubernetes, GitLab CI/CD etc.

# Technologies

- Technologies: Java, Scala, Python.
- Web: HTML/CSS/JS, Bootstrap.
- Container Technology and Management: Kubernetes, Docker, Unit Testing, OOPS, Backend
- Tools and Frameworks: AWS, GitLab, Jenkins, Grafana, Spring, Splunk, PostgreSQL, Jira, Tableau.


# Extra-Curricular Activites

- A member of NSS(National Service Scheme) for 2 years (2016 to 2018) which comes under Govt. of India Ministry of Youth Affairs & Sports. Being a volunteer, I participated in many social service activities like beach clean-up drive, attended special 8 days NSS camp, visit to multiple old age homes.
- Coach for 'Football is Life' project which runs under agreement with Atletico de Madrid Football Club and Gulmohar Center in Mumbai. I trained 240 kids and teens for 60+ hours in soccer, improving their health, psychological, social behaviour and teamwork. I received the certificates from the director of this NGO from Spain.


# Free-Time

To refresh myself from work I play Football or simply go for a walk.
I am a sports enthusiast, lets talk about Cricket, F1 and Football.
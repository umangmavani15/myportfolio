---
title: "Project" # in any language you want
layout: "project" # is necessary
language: en
url: "/projects"
summary: Projects

comments: false
---

Hello Everyone, these are few of the many projects i have built and developed. 

## Camp+

![Camp+](/img/camp+.png)

The application Camp+ which is built in Flutter and NodeJS manages deliveries, visitors to the campus, helps the residents of the hostel in viewing and calling other residents of different hostels, allows booking housekeeping facilities and solves several other essential needs relevant to residential campuses like library booking, notice board management, cafeteria slot management, event booking management, room booking management, laundromat slot booking, canteen delivery system and so on.
I worked in the backend which included working with NodeJS in compliance with the MVC module structure for developing various modules for the features. AWS services like S3 and DynamoDB and also MongoDB were used for data storage. For more information please check out the [Camp+](https://camplus.network/) website.

## Chitran

The “Chitran” platform allows the user to view the various crime data segregated in a very structured manner. As a part of this work, I facilitated the automation of cleaning huge chunks of data provided in the National Crime Records Bureau (NCRB), GoI website. Furthermore, after the cleaning I had to label encode the data for converting them into the machine readable form. Eventually after the preprocessing of the data it was time to visualize the data into various charts which are very easy to understand.

## Sarathi (Safe Zone Detection for UAV)

I worked on the safety and backup mechanisms of the Unmanned Aerial Vehicles (UAV) in case of crash landings, loss of connection, system failure, loss of control etc. The system uses compression and sensing techniques to detect a safe zone using HD cameras fitted onto the UAVs. Compressed Sensing will benefit us in saving Power, low size of the processed image and efficient use of Bandwidth for communication. I worked on the Gabor filters, Compressed Sensing, Markov Chain Code and Chi Square Techniques.

## Terra&Tech (ML-based house predictor)

![Terra N Tech](/img/terrantech.png)

It is a Machine Learning based house predictor for the people who buy houses taking a Home Loan.

Program is built in python environment using python libraries and machine learning concepts. The dataset is cleeaned repeatedely so that it can be readily used by the python program to apply machine learning algorithms. After going through number of regression techniques , finally multiple linear regression algorithm fit close to the dataset. once we have finished the python program we designed API files for the same and took it to the web interface to be readily used in a new or existing systems. The program takes input of your budget and other financial conditions and then gives you the output of the most suitable assets you may own with other benefits. You can test out the website here: [Terra&Tech](https://codecops-hackathon.herokuapp.com/)

## Fruits Image Classifier using VGG16
![Fruits Image Classifier](/img/image-classifier.png)

I used the Fruits 360 dataset and VGG16 which is a pretrained CNN. Data Augmentation techniques were also implemented in order to fit the model more accurately. The fitting takes a lot of computational power and time.

THe Kaggle notebook for the code can be found here: [Fruits Image Classifier using VGG16](https://www.kaggle.com/niharsanda/fruits-image-classifier-using-vgg16)

## Covid Tracker

![Covid Tracker](/img/covid-tracker.png)

Tracks the information regarding the Covid-19 essential for a student to know before he or she finds a job in the affected places.

For more information about the project and the code you can check out the [Github](https://github.com/umangmavani15/Covid-19-Tracker)

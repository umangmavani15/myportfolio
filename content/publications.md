---
title: "Research Publications" # in any language you want
layout: "publications" # is necessary
language: en
url: "/publications"
summary: My research publications across various conferences and journals.

comments: false
---

## Natural Language Processing based Text Summarization and Querying Model (IEEE)

- Brief Description 

## Naive Bayes Classification on Student Placement Data: A Comparative Study of Data Mining Tools (Springer)

- Brief Description

## A Semester Grade Point Average Estimation System for Students Attaining Higher Schooling in Specialized Courses (IEEE)

- Brief Description


